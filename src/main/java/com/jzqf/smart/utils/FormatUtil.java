package com.jzqf.smart.utils;

import java.nio.charset.Charset;

public class FormatUtil {

    public static byte[] hexString2Bytes(String str) {
        if(str == null || str.trim().equals("")) {
            return new byte[0];
        }

        byte[] bytes = new byte[str.length() / 2];
        for(int i = 0; i < str.length() / 2; i++) {
            String subStr = str.substring(i * 2, i * 2 + 2);
            bytes[i] = (byte) Integer.parseInt(subStr, 16);
        }

        return bytes;
    }

    public static String bytes2HexString(byte[] bytes) {
        StringBuilder buf = new StringBuilder(bytes.length * 2);
        for(byte b : bytes) { // 使用String的format方法进行转换
            buf.append(String.format("%02x", new Integer(b & 0xff)));
        }

        return buf.toString();
    }

    public static String bytes2HexString(byte[] bytes, int len) {
        StringBuilder buf = new StringBuilder(len);
        int i = 0;
        for(byte b : bytes) { // 使用String的format方法进行转换
            i++;
            if(i > len) {
                break;
            }
            buf.append(String.format("%02x", new Integer(b & 0xff)));
        }

        return buf.toString();
    }

    /**
     *
     * @param bRefArr
     * @param offset
     * @param len
     * @param reverse false:低字节
     * @return
     */
    public static int ByteArrayToInt(byte[] bRefArr,int offset,int len, boolean reverse){
        int iOutcome = 0;
        byte bLoop;

        if(reverse){
            for (int i = 0; i < len; i++) {
                bLoop = bRefArr[len+offset-i-1];
                iOutcome += (bLoop & 0xFF) << (8 * i);
            }
        }
        else{ //低字节
            for (int i = 0; i < len; i++) {
                bLoop = bRefArr[offset+i];
                iOutcome += (bLoop & 0xFF) << (8 * i);
            }
        }
        return iOutcome;
    }

    /**
     * 字符串转换为16进制字符串
     *
     * @param s
     * @return
     */
    public static String stringToHexString(String s) {
        String str = "";
        for (int i = 0; i < s.length(); i++) {
            int ch = (int) s.charAt(i);
            String s4 = Integer.toHexString(ch);
            str = str + s4;
        }
        return str+"0a";
    }

    public static String hexStringToString(String s) {
        if (s == null || s.equals("")) {
            return null;
        }
        s = s.replace(" ", "");
        byte[] baKeyword = new byte[s.length() / 2];
        for (int i = 0; i < baKeyword.length; i++) {
            try {
                baKeyword[i] = (byte) (0xff & Integer.parseInt(
                        s.substring(i * 2, i * 2 + 2), 16));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            s = new String(baKeyword, Charset.forName("GBK"));
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return s;
    }

    public static int intMath(Integer integerValue) {
        if (integerValue == null) {
            return 0;
        } else {
            return (int) Math.ceil(integerValue / 4f);
        }
    }
}
