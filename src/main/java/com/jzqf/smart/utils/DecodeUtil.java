package com.jzqf.smart.utils;

import java.util.Base64;

import static sun.misc.Version.print;

public class DecodeUtil {

    public static void decode(String base64Str){
        byte[] decode = Base64.getDecoder().decode(base64Str);
        String result=new String(decode);
        System.out.println(result);
    }
}
