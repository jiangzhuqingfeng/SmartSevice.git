package com.jzqf.smart.param;

/**
 * 分页参数
 * 2022/7/11 18:15
 *
 * @author LiuWeiHao
 */
public class PageParam<T> {
    private Integer pageIndex;

    private Integer pageSize;

    private T filter;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public T getFilter() {
        return filter;
    }

    public void setFilter(T filter) {
        this.filter = filter;
    }
}
