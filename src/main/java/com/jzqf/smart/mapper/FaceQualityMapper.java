package com.jzqf.smart.mapper;

import com.jzqf.smart.entity.FaceQuality;

public interface FaceQualityMapper {
    int insert(FaceQuality record);

    int insertSelective(FaceQuality record);
}