package com.jzqf.smart.mapper;

import com.jzqf.smart.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMgrMapper {
    List<User> queryAllUser();
    List<User> queryByUsername(String account);

    User login(String account,String password);
}