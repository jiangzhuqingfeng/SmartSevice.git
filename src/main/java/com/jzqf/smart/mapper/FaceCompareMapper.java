package com.jzqf.smart.mapper;

import com.jzqf.smart.entity.FaceCompare;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FaceCompareMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(FaceCompare record);

    int insertSelective(FaceCompare record);

    FaceCompare selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(FaceCompare record);

    int updateByPrimaryKey(FaceCompare record);
}