package com.jzqf.smart.mapper;

import com.jzqf.smart.entity.FaceCompareMgr;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 2022/7/8 15:06
 *
 * @author LiuWeiHao
 */
@Mapper
public interface FaceCompareMgrMapper {
    /**
     * 查询是否存在
     *
     * @param cropPic 抠图
     * @return
     */
    FaceCompareMgr selectByCropPic(String cropPic);

    /**
     * 更新记录
     *
     * @param record 记录
     * @return
     */
    int updateByCropPic(FaceCompareMgr record);

    int insertSelective(FaceCompareMgr record);

    List<FaceCompareMgr> selectPage(Integer pageIndex, Integer pageSize, Map map);

//    List<FaceCompareMgr> selectPage(Map map);

    long selectTotalSize(FaceCompareMgr faceCompareMgr);
}
