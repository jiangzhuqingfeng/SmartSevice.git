package com.jzqf.smart.entity;

public class SignBean {
    private long timestamp;

    private String nonceStr;
    private String signature;

    public long getTimestamp() {
        return timestamp;
    }

    public SignBean(long timestamp, String nonceStr, String signature) {
        this.timestamp = timestamp;
        this.nonceStr = nonceStr;
        this.signature = signature;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
