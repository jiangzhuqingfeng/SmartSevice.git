package com.jzqf.smart.entity;

import java.util.Date;

public class FaceCompare {
    private Integer id;

    private String communityName;

    private String staffName;

    private Integer staffNo;

    private String accessName;

    private Float scoreBd;

    private Float scoreSt;

    private Float scoreBdApi;

    private String facePic;

    private String cropPic;

    private Date compareTime;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName == null ? null : communityName.trim();
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName == null ? null : staffName.trim();
    }

    public Integer getStaffNo() {
        return staffNo;
    }

    public void setStaffNo(Integer staffNo) {
        this.staffNo = staffNo;
    }

    public String getAccessName() {
        return accessName;
    }

    public void setAccessName(String accessName) {
        this.accessName = accessName == null ? null : accessName.trim();
    }

    public Float getScoreBd() {
        return scoreBd;
    }

    public void setScoreBd(Float scoreBd) {
        this.scoreBd = scoreBd;
    }

    public Float getScoreSt() {
        return scoreSt;
    }

    public void setScoreSt(Float scoreSt) {
        this.scoreSt = scoreSt;
    }

    public Float getScoreBdApi() {
        return scoreBdApi;
    }

    public void setScoreBdApi(Float scoreBdApi) {
        this.scoreBdApi = scoreBdApi;
    }

    public String getFacePic() {
        return facePic;
    }

    public void setFacePic(String facePic) {
        this.facePic = facePic == null ? null : facePic.trim();
    }

    public String getCropPic() {
        return cropPic;
    }

    public void setCropPic(String cropPic) {
        this.cropPic = cropPic == null ? null : cropPic.trim();
    }

    public Date getCompareTime() {
        return compareTime;
    }

    public void setCompareTime(Date compareTime) {
        this.compareTime = compareTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}