package com.jzqf.smart.entity;

import java.util.List;
/**
 *  分页查询
 * 2022/7/11 17:20
 * @author LiuWeiHao
 */
public class PageResult<T> {
    private  Integer pageIndex;

    private Integer pageSize;

    private Long totalSize;

    private List<T> data;

    public PageResult(Integer pageIndex, Integer pageSize, Long totalSize, List<T> data) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.totalSize = totalSize;
        this.data = data;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
