package com.jzqf.smart.controller;

import com.jzqf.smart.entity.BaseResponse;
import com.jzqf.smart.entity.User;
import com.jzqf.smart.service.SignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class SignController {
    @Autowired
    SignService signService;

    @CrossOrigin
    @RequestMapping(value = "/sign", method = RequestMethod.POST)
    public BaseResponse login(@RequestBody String url) {
        try {
            return new BaseResponse(200, "添加成功", signService.sign(url));
        } catch (Exception e) {
            return new BaseResponse(500, "登录失败" + e.getMessage());
        }
    }
}
