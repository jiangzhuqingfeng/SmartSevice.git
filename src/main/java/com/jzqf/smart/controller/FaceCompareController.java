package com.jzqf.smart.controller;

import com.jzqf.smart.entity.BaseResponse;
import com.jzqf.smart.entity.FaceCompareMgr;
import com.jzqf.smart.entity.PageResult;
import com.jzqf.smart.param.PageParam;
import com.jzqf.smart.service.FaceCompareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 2022/7/8 15:21
 *
 * @author LiuWeiHao
 */
@RestController
public class FaceCompareController {
    @Autowired
    FaceCompareService faceCompareService;

    @CrossOrigin
    @RequestMapping(value = "/faceCompare/add", method = RequestMethod.POST)
    public BaseResponse add(@RequestBody FaceCompareMgr faceCompare) {
        try {
            faceCompareService.addFaceCompare(faceCompare);
            return new BaseResponse(200, "添加成功");
        } catch (Exception e) {
            return new BaseResponse(500, "操作失败" + e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/faceCompare/pageList", method = RequestMethod.POST)
    public BaseResponse pageList(@RequestBody PageParam<FaceCompareMgr> pageParam) {
        try {
            PageResult<FaceCompareMgr> pageResult = faceCompareService.pageList(pageParam);
            return new BaseResponse(200, "操作成功", pageResult);
        } catch (Exception e) {
            return new BaseResponse(500, "操作失败" + e.getMessage());
        }
    }
}
