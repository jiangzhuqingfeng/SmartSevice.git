package com.jzqf.smart.service;

import com.jzqf.smart.entity.User;

import java.util.List;

public interface UserService {

    List<User> getAllUser();

    void addUser();

    int register(User user) throws Exception;

    User login(User user) throws Exception;
}
