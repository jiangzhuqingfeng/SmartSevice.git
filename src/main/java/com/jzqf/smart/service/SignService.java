package com.jzqf.smart.service;

import com.jzqf.smart.entity.SignBean;

public interface SignService {
    SignBean sign(String url);
}
