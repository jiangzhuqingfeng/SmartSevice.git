package com.jzqf.smart.service.impl;

import com.jzqf.smart.entity.FaceCompareMgr;
import com.jzqf.smart.entity.PageResult;
import com.jzqf.smart.mapper.FaceCompareMapper;
import com.jzqf.smart.mapper.FaceCompareMgrMapper;
import com.jzqf.smart.param.PageParam;
import com.jzqf.smart.service.FaceCompareService;
import com.jzqf.smart.utils.ConvertMapHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 2022/7/8 15:12
 *
 * @author LiuWeiHao
 */
@Service
public class FaceCompareServiceImpl implements FaceCompareService {
    @Autowired
    FaceCompareMapper faceCompareMapper;
    @Autowired
    FaceCompareMgrMapper faceCompareMgrMapper;

    @Override
    public void addFaceCompare(FaceCompareMgr faceCompare) throws Exception {
        if (faceCompare == null) {
            throw new IllegalArgumentException("传入数据为空!");
        }
        if (faceCompare.getCropPic() == null) {
            throw new IllegalArgumentException("cropPic为空!");
        }
        System.out.println(faceCompare.getCropPic());
        FaceCompareMgr faceCompareSelect = faceCompareMgrMapper.selectByCropPic(faceCompare.getCropPic());
        if (faceCompareSelect == null) {
            faceCompareMgrMapper.insertSelective(faceCompare);
        } else {
            faceCompareMgrMapper.updateByCropPic(faceCompare);
        }
    }

    @Override
    public PageResult<FaceCompareMgr> pageList(PageParam<FaceCompareMgr> pageParam) throws Exception {
        Map<String, String> map = ConvertMapHelper.objectToMap(pageParam.getFilter());
        int start = (pageParam.getPageIndex() - 1) * pageParam.getPageSize();
//        map.put("pageIndex", String.valueOf(start));
//        map.put("pageSize", String.valueOf(pageParam.getPageSize()));
//        List<FaceCompareMgr> list = faceCompareMgrMapper.selectPage(map);
        List<FaceCompareMgr> list = faceCompareMgrMapper.selectPage(start, pageParam.getPageSize(), map);
        long totalSize = faceCompareMgrMapper.selectTotalSize(pageParam.getFilter());
        PageResult<FaceCompareMgr> pageResult = new PageResult<>(pageParam.getPageIndex(), pageParam.getPageSize(), totalSize, list);
        return pageResult;
    }
}
