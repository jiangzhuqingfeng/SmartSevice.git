package com.jzqf.smart.service.impl;

import com.jzqf.smart.entity.SignBean;
import com.jzqf.smart.service.SignService;
import org.springframework.stereotype.Service;

@Service
public class SignServiceImpl implements SignService {
    @Override
    public SignBean sign(String url) {
        return new SignBean(1000,"nonceStr","signature");
    }
}
