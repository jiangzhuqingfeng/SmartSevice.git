package com.jzqf.smart.service.impl;

import com.jzqf.smart.entity.User;
import com.jzqf.smart.mapper.UserMapper;
import com.jzqf.smart.mapper.UserMgrMapper;
import com.jzqf.smart.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserMgrMapper userMgrMapper;

    @Override
    public List<User> getAllUser() {
        return userMgrMapper.queryAllUser();
    }

    @Override
    public void addUser() {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setAccount("jiangfeng1");
        user.setPassword("1234561");
        userMapper.insert(user);
    }

    @Override
    public int register(User user) throws Exception {
        if (user == null) {
            throw new IllegalArgumentException("传入数据为空!");
        }
        List<User> users = userMgrMapper.queryByUsername(user.getAccount());
        if (users != null && users.size() > 0) {
            throw new IllegalArgumentException("用户名已存在!");
        }
        user.setId(String.valueOf(System.currentTimeMillis()));
        return userMapper.insert(user);
    }

    @Override
    public User login(User user) throws Exception {
        if (user == null) {
            throw new IllegalArgumentException("传入数据为空!");
        }
        User result = userMgrMapper.login(user.getAccount(), user.getPassword());
        if (result == null) {
            throw new Exception("用户或密码不对");
        }
//        result.setToken(TokenUtil.token(result.getAccount(),result.getPassword()));
        return result;
    }
}
