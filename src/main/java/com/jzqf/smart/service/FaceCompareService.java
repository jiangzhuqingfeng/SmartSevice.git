package com.jzqf.smart.service;

import com.jzqf.smart.entity.FaceCompareMgr;
import com.jzqf.smart.entity.PageResult;
import com.jzqf.smart.param.PageParam;

/**
 * 2022/7/8 15:10
 *
 * @author LiuWeiHao
 */
public interface FaceCompareService {
    /**
     * 添加对比记录
     *
     * @param faceCompare 对比记录
     * @throws Exception
     */
    void addFaceCompare(FaceCompareMgr faceCompare) throws Exception;

    /**
     * 分页查询
     *
     * @param pageParam
     * @return
     * @throws Exception
     */
    PageResult<FaceCompareMgr> pageList(PageParam<FaceCompareMgr> pageParam) throws Exception;
}
