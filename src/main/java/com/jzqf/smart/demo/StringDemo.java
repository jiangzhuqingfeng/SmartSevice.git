package com.jzqf.smart.demo;

import com.jzqf.smart.utils.ConvertMapHelper;
import com.jzqf.smart.utils.FormatUtil;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Map;

public class StringDemo {
    public static void main(String[] args) {
       String value="0012a485-f519-44e7-9db4-9ebde78eb5cd";
       System.out.println(value.toUpperCase());

    }

    static void test485() {
        final String init = "{\"factory\":\"insprid\",\"product\":\"SYZNGW00\",\"sn\":\"6666666666\",\"service\":\"init\",\"name\":\"wangguan\"}";
        System.out.println(FormatUtil.stringToHexString(init));
        System.out.println("device指令");
        String jsonDevice = "{\"factory\":\"insprid\",\"product\":\"SYZNGW00\",\"sn\":\"6666666666\",\"service\":\"device\",\"devices\":[{\"id\":15,\"name\":\"masterAir\",\"type\":\"airConditioner\",\"product\":\"air\",\"attribute\":{\"switch\":\"on\",\"mode\":\"cool\",\"speed\":\"low\",\"currentTemperature\":26.5}},{\"id\":16,\"name\":\"sittingHeating\",\"type\":\"floorHeating\",\"product\":\"heating\",\"attribute\":{\"switch\":\"on\"}},{\"id\":17,\"name\":\"sittingFreshAir\",\"type\":\"FAU\",\"product\":\"wind\",\"attribute\":{\"switch\":\"on\",\"speed\":\"low\"}},{\"id\":18,\"name\":\"sittingAirbox\",\"type\":\"Airbox\",\"product\":\"box\",\"attribute\":{\"switch\":\"on\",\"currentHumidity\":68,\"HCHO\":0.08,\"CO2\":14,\"currentTemperature\":26.3,\"PM10\":34,\"PM25\":34,\"AQI\":85}}]}";
        System.out.println(FormatUtil.stringToHexString(jsonDevice));
        System.out.println("新风");
        String jsonFAU = "{\"factory\":\"insprid\",\"product\":\"SYZNGW00\",\"sn\":\"5b3d9e68136411d765d04a77e4137960\",\"service\":\"report\",\"device\":{\"id\":17,\"name\":\"............\",\"type\":\"FAU\",\"product\":\"SYZNGW00\",\"attribute\":{\"switch\":\"on\"}}}";
        System.out.println(FormatUtil.stringToHexString(jsonFAU));
        System.out.println("空气盒子");
        String jsonAirbox = "{\"factory\":\"insprid\",\"product\":\"SYZNGW00\",\"sn\":\"5b3d9e68136411d765d04a77e4137960\",\"service\":\"report\",\"device\":{\"id\":18,\"name\":\"............\",\"type\":\"Airbox\",\"product\":\"SYZNGW00\",\"attribute\":{\"currentTemperature\":27.333}}}";
        System.out.println(FormatUtil.stringToHexString(jsonAirbox));
     

        String hexString="12ED9BEACABA1469D4685BB66B1ACAF4B4D4E9D3B7DAB7B414E95351A7D1B7B1B7565369A9AC5C5469F6B63BDBCB34695BB6CBAAF6D6DA76F62CDA9ACAB6BAAA36F656CAAACAD6D6EA76EACAF676AA6AD6F6B63BDBCB54DA34D429AADA5BB67BAAEADA74B4AAEABAB42BB4CF";
        System.out.println(FormatUtil.hexStringToString(hexString));
    }

    static void jsonToDartProperty() {
        String jsonStr = "";
        Model model = new Model();
        Map<String, String> stringStringMap = ConvertMapHelper.objectSelefToMap(model);
        for (String key : stringStringMap.keySet()) {
            System.out.println(String.format(" const String %s='%s';", key,
                    stringStringMap.get(key)));
        }
    }

    static void inet() {
        try {
            InetAddress localHost = InetAddress.getLocalHost(); //本机
            System.out.println("本机IP地址：" + localHost.getHostAddress());
            System.out.println("本机名称：" + localHost.getHostName());

            //根据域名得到InetAddress对象
            InetAddress bd = InetAddress.getByName("my.zerotier.com");
            System.out.println("百度服务器地址：" + bd.getHostAddress() + ",port=" + new String(bd.getAddress()));
            System.out.println("百度服务器名称：" + bd.getHostName());


            //根据IP地址得到InetAddress对象
            InetAddress ia = InetAddress.getByName("10.144.51.206");
            System.out.println("服务器主机IP：" + ia.getHostAddress());
            //如果39.130.131.42IP地址不存在或者DNS（域名解析系统）不允许进行IP地址和域名的映射，就会直接返回域名地址
            System.out.println("主机名称" + ia.getHostName());

            InetSocketAddress inetSocketAddress = new InetSocketAddress("10.144.51.206", 8028);
            System.out.println("\n getHostName:" + inetSocketAddress.getHostName());
            System.out.println("getHostString:" + inetSocketAddress.getHostString());
            System.out.println("getAddress:" + inetSocketAddress.getAddress());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static void sub(String value) {
        System.out.println(-1 & 255);
        System.out.println(9 & 0xff);
        String path = "/face/2020-10/22/1603367802034.jpg";
        System.out.println(path.substring(path.lastIndexOf("/") + 1));


        if (value.contains("_")) {
            String[] split = value.split("_");
            System.out.println(split[1]);
        }

        String runId = Long.toHexString(System.currentTimeMillis());
        String newMemberAddress = runId.substring(runId.length() - 10);
        System.out.println(runId + "\n" + newMemberAddress);
        System.out.println(System.getenv("VufSZSaF2D375wpFd4ydxgiyLdO2EEJH"));
    }

    static void ticket() {
        //1234567
        long first = 5000000;//7
        long second = 500000;//6
        long third = 1800;//5
        long fourth = 300;//4
        long fifth = 20;//3
        long sixth = 5;//2
        long total = (long) Math.pow(10, 7);
        double sum = 0;
        sum += 1f / total * first;
        sum += (2f / total) * second;
        sum += (3f / total) * third;
        sum += (4f / total) * fourth;
        sum += (5f / total) * fifth;
        sum += (6f / total) * sixth;
        System.out.println(total);
        System.out.println(sum);
    }


}
