package com.jzqf.smart.demo;

/**
 * 实体类model
 */
public class Model {
 /**
  * 检测时间
  */
 private String uploadTime;
 private Integer staffNo;
 private String staffName;
 /**
  * 质量检测
  */
 private Integer quality;
 /**
  * kb
  */
 private Double imgLength;
 /**
  * 图片分辨率 2000*2667
  */
 private String imgSize;
 /**
  * 人脸占比
  */
 private Double faceScale;
 private String faceUrl;
}
