package com.jzqf.test.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 */
public class FileUtil {
    //        List<String> fileList= FileUtil.getFiles("D:\\Develop\\MinProgram\\p.operate.mini\\src\\pages");
//        if (fileList != null) {
//            for(String name:fileList){
//                    System.out.println("\""+name+"\""+",");
//            }
//        }
    public static List<String> getFiles(String directoryPath){
        File file = new File(directoryPath);
        if (!file.exists()) {
            return null;
        }
        List<String> fileNames = new ArrayList<>();
        return getFileNames("pages",file, fileNames);
    }
    private static List<String> getFileNames(String parentName,File file, List<String> fileNames) {
        File[] files = file.listFiles();
//        System.out.println("---------"+file.getName());
        for (File f : Objects.requireNonNull(files)) {
            if (f.isDirectory()) {
                getFileNames(parentName+"/"+f.getName(),f, fileNames);
            } else {
                if(f.getName().endsWith(".vue")){
                    String name=f.getName().substring(0,f.getName().length()-4);
                    fileNames.add(parentName+"/"+name);
                }
            }
        }
        return fileNames;
    }
}
