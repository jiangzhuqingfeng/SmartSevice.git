package com.jzqf.test;

import java.io.File;

public class Demo {
    public static void main(String[] args) {
        String path = "D:/"; // D盘路径
        File directory = new File(path);
        findImages(directory);
    }

    private static String hotTopicName(String name) {
        String hotTopicName = name;
        if (hotTopicName.startsWith("#")) {
            hotTopicName = hotTopicName.substring(1, hotTopicName.length() - 1);
        }
        if (hotTopicName.endsWith("#")) {
            hotTopicName = hotTopicName.substring(0, hotTopicName.length() - 2);
        }
        return hotTopicName;
    }
    //查找D盘下所有图片
    public static void findImages(File directory) {
        // 获取目录下的所有文件和文件夹
        File[] files = directory.listFiles();

        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    // 递归调用findImages方法处理子文件夹
                    findImages(file);
                } else {
                    // 检查文件的扩展名是否为图片格式
                    String fileName = file.getName();
                    String extension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
                    if (extension.equals("jpg") || extension.equals("png") || extension.equals("gif")) {
                        // 打印图片文件路径
                        System.out.println(file.getAbsolutePath());
                    }
                }
            }
        }
    }
}
